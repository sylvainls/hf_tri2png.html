# hf_tri2pfg.html - Introduction

hf_tri2png.html is a standalone HTML/ECMAScript application to create a
heightfield image from a triangles.

It takes a text file with space-separated triangle coordinates on each line
and transforms it in a greyscale PNG image file of the Z coordinates.

By “standalone application” we mean that the user just needs to open the file
hf_tri2png.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.


hf_tri2png.html is licenced under GPLv3+.


# Requirements

* NONE!


# Usage

Just copy the files on your computer and open hf_tri2png.html in a web browser.
Select the files to transform and then download the results.
