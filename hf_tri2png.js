/*
 *  Copyright 2020  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

// Math stuff
function vec_add( u, v ) {
    return [ u[0] + v[0], u[1] + v[1], u[2] + v[2] ];
}

function vec_sub( u, v ) {
    return [ u[0] - v[0], u[1] - v[1], u[2] - v[2] ];
}

function vec_normalize( v ) {
    const d = Math.sqrt( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] );
    return [ v[0] / d, v[1] / d, v[2] / d ];
}

function vec_cross( u, v ) {
    return [
        u[1] * v[2] - u[2] * v[1],
        u[2] * v[0] - u[0] * v[2],
        u[0] * v[1] - u[1] * v[0]
    ];
}

function vec_dot( u, v ) {
    return u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
}


/* Method “Raster” */

// stores min and max x on y-lines
class MinMax {
    constructor( min, max ) {
        this.min = min;
        this.max = max;
        this.min_z = 0;
        this.max_z = 0;
    }

    update( v, z ) {
        if ( v < this.min ) {
            this.min = v;
            this.min_z = z;
        }
        if ( v > this.max ) {
            this.max = v;
            this.max_z = z;
        }
    }
};

class Contour {

    constructor( min, max ) {
        this.min = min;
        this.max = max;
        this.size = max - min + 1;
        this.lines = [];
        for ( let i = 0; i < this.size; i++ ) {
            this.lines[i] = new MinMax( Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER );
        }
    }

    update( i, v, z ) {
        if ( i >= this.min && i <= this.max ) {
            this.lines[i - this.min].update( v, z );
        }
    }

    get_line( i ) {
        return this.lines[i - this.min];
    }

}

// draw the line p0-p1
// Amanatides & Woo
function draw_line( p0, p1 ) {
    let result = [];

    let x = Math.floor( p0[0] );
    let y = Math.floor( p0[1] );
    let z = Math.floor( p0[2] );
    const xf = Math.floor( p1[0] );
    const yf = Math.floor( p1[1] );
    const zf = Math.floor( p1[2] );

    // first point is always in
    result.push( [ x, y, z ] );

    // direction
    const ray = vec_normalize( vec_sub( p1, p0 ) );
    // steps
    const s_x = ray[0] > 0.0 ? 1 : -1;
    const s_y = ray[1] > 0.0 ? 1 : -1;
    const s_z = ray[2] > 0.0 ? 1 : -1;

    // next border intersection (frac part of p0)
    const bi_x = s_x > 0 ? ( x + 1.0 ) - p0[0] : p0[0] - x;
    const bi_y = s_y > 0 ? ( y + 1.0 ) - p0[1] : p0[1] - y;
    const bi_z = s_z > 0 ? ( z + 1.0 ) - p0[2] : p0[2] - z;

    // distance to next intersection
    let t_max_x = ray[0] == 0.0 ? Infinity : bi_x / ray[0]; // -Infinity
    let t_max_y = ray[1] == 0.0 ? Infinity : bi_y / ray[1]; //  wouldn’t
    let t_max_z = ray[2] == 0.0 ? Infinity : bi_z / ray[2]; //   be good

    // distance before next voxel
    const dx = ray[0] == 0.0 ? Infinity : 1.0 / ray[0]; // -Infinity
    const dy = ray[1] == 0.0 ? Infinity : 1.0 / ray[1]; //  wouldn’t
    const dz = ray[2] == 0.0 ? Infinity : 1.0 / ray[2]; //   be good

    // main loop
    // limit = Manhattan distance
    const steps = Math.abs( xf - x ) + Math.abs( yf - y ) + Math.abs( zf - z );
    for ( let i = 0; i < steps; ++i ) {
        if ( Math.abs( t_max_x ) < Math.abs( t_max_y ) ) {
            if ( Math.abs( t_max_x ) < Math.abs( t_max_z ) ) {
                x += s_x;
                t_max_x += dx;
            } else {
                z += s_z;
                t_max_z += dz;
            }
        } else {
            if ( Math.abs( t_max_y ) < Math.abs( t_max_z ) ) {
                y += s_y;
                t_max_y += dy;
            } else {
                z += s_z;
                t_max_z += dz;
            }
        }
        result.push( [ x, y, z ] );
    }
    return result;
}

/* find voxels occupied by triangle tri of normal norm0
 *
 *      A             []          x
 *     /\     ->     [-]    ->   xxx
 *   B/__\          [---]       xxxxx
 *       C            [-]         xxx
 *   points        contour     voxels
 */
function fill_triangle( tri, norm0, ymin, ymax ) {
    // find the contour of the triangle
    const contour = new Contour( ymin, ymax );
    for ( let i = 0; i < 3; ++i ) {
        const edge = draw_line( tri[i], tri[ ( i + 1 ) % 3 ] );
        for ( let vox of edge ) {
            contour.update( vox[1], vox[0], vox[2] );
        }
    }

    // fill the triangle
    let result = [];
    for ( let y = ymin; y <= ymax; ++y ) {
        const line = contour.get_line( y );
        result = result.concat( draw_line( [ line.min, y, line.min_z ], [ line.max, y, line.max_z ] ) );
    }
    return result;
}

function voxelize_triangle( triangle, width, length, height, submethod ) {
    // voxels conrner
    const corner = [ -0.5, -0.5, -0.5 ];
    const tri = [
        vec_sub( triangle[0], corner ),
        vec_sub( triangle[1], corner ),
        vec_sub( triangle[2], corner )
    ];

    // bounding box
    let min = [ width, length, height ];
    let max = [ 0, 0, 0 ];
    for ( let i = 0; i < 3; ++i ) {
        for ( let j = 0; j < 3; ++j ) {
            const v = Math.floor( tri[i][j] );
            if ( v < min[j] ) min[j] = v;
            if ( v > max[j] ) max[j] = v;
        }
    }
    for ( let j = 0; j < 3; ++j ) {
        if ( min[j] < 0 ) min[j] = 0;
    }
    if ( max[0] >=  width ) max[0] =  width - 1;
    if ( max[1] >= length ) max[1] = length - 1;
    if ( max[2] >= height ) max[2] = height - 1;

    // small triangle
    if ( min[0] == max[0] && min[1] == max[1] && min[2] == max[2] ) {
        return [ min ];
    }

    // edges
    const edge = [
        vec_sub( tri[1], tri[0] ),
        vec_sub( tri[2], tri[1] ),
        vec_sub( tri[0], tri[2] )
    ];

    // normal on tri[0]
    const norm0 = vec_normalize( vec_cross( edge[0], edge[2] ) );

    // find longest normal…
    const fx = Math.abs( norm0[0] );
    const fy = Math.abs( norm0[1] );
    const fz = Math.abs( norm0[2] );
    const which_way = fx > fy ? ( fx > fz ? 0 : 2 ) : ( fy > fz ? 1 : 2 );

    // …use it as Z
    let result = [];
    if ( which_way == 2 ) { // Z

        for ( let vox of fill_triangle( tri, norm0, min[1], max[1] ) ) {
            if ( vox[0] >= min[0] && vox[0] <= max[0] &&
                 vox[1] >= min[1] && vox[1] <= max[1] &&
                 vox[2] >= min[2] && vox[2] <= max[2] ) {
                result.push( [ vox[0], vox[1], vox[2] ] );
            }
        }

    } else if ( which_way == 1 ) { // Y

        // rotate to use X as Z
        const rit = [
            [ tri[0][1], tri[0][2], tri[0][0] ],
            [ tri[1][1], tri[1][2], tri[1][0] ],
            [ tri[2][1], tri[2][2], tri[2][0] ]
        ];
        const norm = [ norm0[1], norm0[2], norm0[0] ];

        // compute & rotate back the results
        for ( let vox of fill_triangle( rit, norm, min[2], max[2] ) ) {
            if ( vox[2] >= min[0] && vox[2] <= max[0] &&
                 vox[0] >= min[1] && vox[0] <= max[1] &&
                 vox[1] >= min[2] && vox[1] <= max[2] ) {
                result.push( [ vox[2], vox[0], vox[1] ] );
            }
        }

    } else if ( which_way == 0 ) { // X

        // rotate to use Y as Z
        const itr = [
            [ tri[0][2], tri[0][0], tri[0][1] ],
            [ tri[1][2], tri[1][0], tri[1][1] ],
            [ tri[2][2], tri[2][0], tri[2][1] ]
        ];
        const norm = [ norm0[2], norm0[0], norm0[1] ];

        // compute & rotate back the results
        for ( let vox of fill_triangle( itr, norm, min[0], max[0] ) ) {
            if ( vox[1] >= min[0] && vox[1] <= max[0] &&
                 vox[2] >= min[1] && vox[2] <= max[1] &&
                 vox[0] >= min[2] && vox[0] <= max[2] ) {
                result.push( [ vox[1], vox[2], vox[0] ] );
            }
        }

    }
    return result;
}


/* Method “Akenine-Möller”
 *
 * http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/
 *
 * AABB-triangle overlap test code
 * by Tomas Akenine-Möller
 * Acknowledgement: Many thanks to Pierre Terdiman for
 * suggestions and discussions on how to optimize code.
 * Thanks to David Hunt for finding a ">="-bug!
 */

function plane_box_overlap( normal, p ) {
    let vmin = [ 0, 0, 0 ];
    let vmax = [ 0, 0, 0 ];
    for ( let i = 0; i < 3; ++i ) {
        if ( normal[i] > 0.0 ) {
            vmin[i] = -0.5 - p[i];
            vmax[i] =  0.5 - p[i];
        } else {
            vmin[i] =  0.5 - p[i];
            vmax[i] = -0.5 - p[i];
        }
    }
    if ( vec_dot( normal, vmin ) > 0.0 ) return false;
    if ( vec_dot( normal, vmax ) >= 0.0 ) return true;
    return false;
}

function tri_box_overlap( v, e ) {
    // use separating axis theorem to test overlap between triangle and box
    // need to test for overlap in these directions:
    // 1) the {x,y,z}-directions (actually, since we use the AABB of the triangle
    //    we do not even need to test these)
    // 2) normal of the triangle
    // 3) crossproduct(edge from tri, {x,y,z}-direction)
    //    this gives 3x3=9 more tests

    // Bullet 3:
    //  test the 9 tests first (this was faster)
    let fex = Math.abs( e[0][0] );
    let fey = Math.abs( e[0][1] );
    let fez = Math.abs( e[0][2] );

    let p0 = e[0][2] * v[0][1] - e[0][1] * v[0][2];
    let p2 = e[0][2] * v[2][1] - e[0][1] * v[2][2];
    let min = 0;
    let max = 0;
    if ( p0 < p2 ) {
        min = p0;
        max = p2;
    } else {
        min = p2;
        max = p0;
    }
    let rad = fez * 0.5 + fey * 0.5;
    if ( min > rad || max < -rad ) return false;

    p0 = -e[0][2] * v[0][0] + e[0][0] * v[0][2];
    p2 = -e[0][2] * v[2][0] + e[0][0] * v[2][2];
    if ( p0 < p2 ) {
        min = p0;
        max = p2;
    } else {
        min = p2;
        max = p0;
    }
    rad = fez * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    let p1 = e[2][1] * v[1][0] - e[2][0] * v[1][1];
    p2 = e[2][1] * v[2][0] - e[2][0] * v[2][1];
    if ( p2 < p1 ) {
        min = p2;
        max = p1;
    } else {
        min = p1;
        max = p2;
    }
    rad = fey * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    fex = Math.abs( e[1][0] );
    fey = Math.abs( e[1][1] );
    fez = Math.abs( e[1][2] );

    p0 = e[1][2] * v[0][1] - e[1][1] * v[0][2];
    p2 = e[1][2] * v[2][1] - e[1][1] * v[2][2];
    if ( p0 < p2 ) {
        min = p0;
        max = p2;
    } else {
        min = p2;
        max = p0;
    }
    rad = fez * 0.5 + fey * 0.5;
    if ( min > rad || max < -rad ) return false;

    p0 = -e[1][2] * v[0][0] + e[1][0] * v[0][2];
    p2 = -e[1][2] * v[2][0] + e[1][0] * v[2][2];
    if ( p0 < p2 ) {
        min = p0;
        max = p2;
    } else {
        min = p2;
        max = p0;
    }
    rad = fez * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    p0 = e[1][1] * v[0][0] - e[1][0] * v[0][1];
    p1 = e[1][1] * v[1][0] - e[1][0] * v[1][1];
    if ( p0 < p1 ) {
        min = p0;
        max = p1;
    } else {
        min = p1;
        max = p0;
    }
    rad = fey * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    fex = Math.abs( e[2][0] );
    fey = Math.abs( e[2][1] );
    fez = Math.abs( e[2][2] );

    p0 = e[2][2] * v[0][1] - e[2][1] * v[0][2];
    p1 = e[2][2] * v[1][1] - e[2][1] * v[1][2];
    if ( p0 < p1 ) {
        min = p0;
        max = p1;
    } else {
        min = p1;
        max = p0;
    }
    rad = fez * 0.5 + fey * 0.5;
    if ( min > rad || max < -rad ) return false;

    p0 = -e[2][2] * v[0][0] + e[2][0] * v[0][2];
    p1 = -e[2][2] * v[1][0] + e[2][0] * v[1][2];
    if ( p0 < p1 ) {
        min = p0;
        max = p1;
    } else {
        min = p1;
        max = p0;
    }
    rad = fez * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    p1 = e[2][1] * v[1][0] - e[2][0] * v[1][1];
    p2 = e[2][1] * v[2][0] - e[2][0] * v[2][1];
    if ( p2 < p1 ) {
        min = p2;
        max = p1;
    } else {
        min = p1;
        max = p2;
    }
    rad = fey * 0.5 + fex * 0.5;
    if ( min > rad || max < -rad ) return false;

    // Bullet 1:
    //  first test overlap in the {x,y,z}-directions
    //  find min, max of the triangle each direction, and test for overlap in
    //  that direction -- this is equivalent to testing a minimal AABB around
    //  the triangle against the AABB

    // test in X-direction
    min = Math.min( v[0][0], v[1][0], v[2][0] );
    max = Math.max( v[0][0], v[1][0], v[2][0] );
    if ( min > 0.5 || max < -0.5 ) return false;

    // test in Y-direction
    min = Math.min( v[0][1], v[1][1], v[2][1] );
    max = Math.max( v[0][1], v[1][1], v[2][1] );
    if ( min > 0.5 || max < -0.5 ) return false;

    // test in Z-direction
    min = Math.min( v[0][2], v[1][2], v[2][2] );
    max = Math.max( v[0][2], v[1][2], v[2][2] );
    if ( min > 0.5 || max < -0.5 ) return false;

    // Bullet 2:
    //  test if the box intersects the plane of the triangle
    //  compute plane equation of triangle: normal*x+d=0
    let normal = vec_cross( e[0], e[1] );
    if ( ! plane_box_overlap( normal, v[0] ) ) return false;

    return true;   // box and triangle overlaps
}

/* Method “Voorhies”
 *
 * http://www.realtimerendering.com/resources/GraphicsGems/gemsiii/triangleCube.c
 *
 * cube triangle intersection
 */

const EPSILON = 10e-5;

function sign3( v ) {
    return ( v[0] < EPSILON ? 0x04 : 0 ) | ( v[0] > -EPSILON ? 0x20 : 0 ) |
        ( v[1] < EPSILON ? 0x02 : 0 ) | ( v[1] > -EPSILON ? 0x10 : 0 ) |
        ( v[2] < EPSILON ? 0x01 : 0 ) | ( v[2] > -EPSILON ? 0x08 : 0 );
}

function lerp( a, b, c ) {
    return b + a * ( c - b );
}

// Which of the six face-plane(s) is point P outside of?
function face_plane( p ) {
    let outcode = 0;
    if ( p[0] >  0.5 ) outcode |= 0x01;
    if ( p[0] < -0.5 ) outcode |= 0x02;
    if ( p[1] >  0.5 ) outcode |= 0x04;
    if ( p[1] < -0.5 ) outcode |= 0x08;
    if ( p[2] >  0.5 ) outcode |= 0x10;
    if ( p[2] < -0.5 ) outcode |= 0x20;
    return outcode;
}

// Which of the twelve edge plane(s) is point P outside of?
function bevel_2d( p ) {
    let outcode = 0;
    if ( p[0] + p[1] > 1.0 ) outcode |= 0x001;
    if ( p[0] - p[1] > 1.0 ) outcode |= 0x002;
    if (-p[0] + p[1] > 1.0 ) outcode |= 0x004;
    if (-p[0] - p[1] > 1.0 ) outcode |= 0x008;
    if ( p[0] + p[2] > 1.0 ) outcode |= 0x010;
    if ( p[0] - p[2] > 1.0 ) outcode |= 0x020;
    if (-p[0] + p[2] > 1.0 ) outcode |= 0x040;
    if (-p[0] - p[2] > 1.0 ) outcode |= 0x080;
    if ( p[1] + p[2] > 1.0 ) outcode |= 0x100;
    if ( p[1] - p[2] > 1.0 ) outcode |= 0x200;
    if (-p[1] + p[2] > 1.0 ) outcode |= 0x400;
    if (-p[1] - p[2] > 1.0 ) outcode |= 0x800;
    return outcode;
}

// Which of the eight corner plane(s) is point P outside of?
function bevel_3d( p ) {
    let outcode = 0;
    if (  p[0] + p[1] + p[2] > 1.5 ) outcode |= 0x01;
    if (  p[0] + p[1] - p[2] > 1.5 ) outcode |= 0x02;
    if (  p[0] - p[1] + p[2] > 1.5 ) outcode |= 0x04;
    if (  p[0] - p[1] - p[2] > 1.5 ) outcode |= 0x08;
    if ( -p[0] + p[1] + p[2] > 1.5 ) outcode |= 0x10;
    if ( -p[0] + p[1] - p[2] > 1.5 ) outcode |= 0x20;
    if ( -p[0] - p[1] + p[2] > 1.5 ) outcode |= 0x40;
    if ( -p[0] - p[1] - p[2] > 1.5 ) outcode |= 0x80;
    return outcode;
}

// Test the point "alpha" of the way from P1 to P2
// See if it is on a face of the cube
// Consider only faces in "mask"
function check_point( p1, p2, alpha, mask ) {
    const plane_point = [ lerp( alpha, p1[0], p2[0] ),
                          lerp( alpha, p1[1], p2[1] ),
                          lerp( alpha, p1[2], p2[2] ) ];
    return ( face_plane( plane_point ) & mask ) == 0;
}

// Compute intersection of P1 --> P2 line segment with face planes
// Then test intersection point to see if it is on cube face
// Consider only face planes in "outcode_diff"
// Note: Zero bits in "outcode_diff" means face line is outside of
function check_line( p1, p2, outcode_diff ) {
    if ( ( 0x01 & outcode_diff ) != 0 &&
         check_point( p1, p2, ( 0.5 - p1[0] ) / ( p2[0] - p1[0] ), 0x3e ) ) return true;
    if ( ( 0x02 & outcode_diff ) != 0 &&
         check_point( p1, p2, (-0.5 - p1[0] ) / ( p2[0] - p1[0] ), 0x3d ) ) return true;
    if ( ( 0x04 & outcode_diff ) != 0 &&
         check_point( p1, p2, ( 0.5 - p1[1] ) / ( p2[1] - p1[1] ), 0x3b ) ) return true;
    if ( ( 0x08 & outcode_diff ) != 0 &&
         check_point( p1, p2, (-0.5 - p1[1] ) / ( p2[1] - p1[1] ), 0x37 ) ) return true;
    if ( ( 0x10 & outcode_diff ) != 0 &&
         check_point( p1, p2, ( 0.5 - p1[2] ) / ( p2[2] - p1[2] ), 0x2f ) ) return true;
    if ( ( 0x20 & outcode_diff ) != 0 &&
         check_point( p1, p2, (-0.5 - p1[2] ) / ( p2[2] - p1[2] ), 0x1f ) ) return true;
    return false;
}

// Test if 3D point is inside 3D triangle
function point_triangle_intersection( p, t, e ) {
    // First, a quick bounding-box test:
    // If P is outside triangle bbox, there cannot be an intersection.
    if ( p[0] > Math.max( t[0][0], t[1][0], t[2][0] ) ) return false;
    if ( p[1] > Math.max( t[0][1], t[1][1], t[2][1] ) ) return false;
    if ( p[2] > Math.max( t[0][2], t[1][2], t[2][2] ) ) return false;
    if ( p[0] < Math.min( t[0][0], t[1][0], t[2][0] ) ) return false;
    if ( p[1] < Math.min( t[0][1], t[1][1], t[2][1] ) ) return false;
    if ( p[2] < Math.min( t[0][2], t[1][2], t[2][2] ) ) return false;

    // For each triangle side, make a vector out of it by subtracting vertexes;
    // make another vector from one vertex to point P.
    // The crossproduct of these two vectors is orthogonal to both and the
    // signs of its X,Y,Z components indicate whether P was to the inside or
    // to the outside of this triangle side.

    // Extract X, Y, Z signs as 0..7 or 0...63 integer
    let sign = [ 0, 0, 0 ];
    for ( let i = 0; i < 3; ++i ) {
        const cross = vec_cross( e[i], vec_sub( t[i], p ) );
        sign[i] = sign3( cross );
    }

    // If all three crossproduct vectors agree in their component signs,
    // then the point must be inside all three.
    // P cannot be OUTSIDE all three sides simultaneously.
    return ( sign[0] & sign[1] & sign[2] ) != 0;
}

// This is the main algorithm procedure.
// Triangle t is compared with a unit cube,
// centered on the origin.
// It returns true if t intersects the cube.
function t_c_intersection( t, e ) {
    // First compare all three vertexes with all six face-planes
    // If any vertex is inside the cube, return immediately!
    let v1_test = face_plane( t[0] );
    if ( v1_test == 0 ) return true;

    let v2_test = face_plane( t[1] );
    if ( v2_test == 0 ) return true;

    let v3_test = face_plane( t[2] );
    if ( v3_test == 0 ) return true;

    // If all three vertexes were outside of one or more face-planes,
    // return immediately with a trivial rejection!
    if ( ( v1_test & v2_test & v3_test ) != 0 ) return false;

    // Now do the same trivial rejection test for the 12 edge planes
    v1_test |= bevel_2d( t[0] ) << 8;
    v2_test |= bevel_2d( t[1] ) << 8;
    v3_test |= bevel_2d( t[2] ) << 8;
    if ( ( v1_test & v2_test & v3_test ) != 0 ) return false;

    // Now do the same trivial rejection test for the 8 corner planes
    v1_test |= bevel_3d( t[0] ) << 24;
    v2_test |= bevel_3d( t[1] ) << 24;
    v3_test |= bevel_3d( t[2] ) << 24;
    if ( ( v1_test & v2_test & v3_test ) != 0 ) return false;

    // If vertex 1 and 2, as a pair, cannot be trivially rejected
    // by the above tests, then see if the v1-->v2 triangle edge
    // intersects the cube.  Do the same for v1-->v3 and v2-->v3.
    // Pass to the intersection algorithm the "OR" of the outcode
    // bits, so that only those cube faces which are spanned by
    // each triangle edge need be tested.
    if ( ( v1_test & v2_test ) == 0 &&
         check_line( t[0], t[1], v1_test | v2_test ) ) return true;
    if ( ( v1_test & v3_test ) == 0 &&
         check_line( t[0], t[2], v1_test | v3_test ) ) return true;
    if ( ( v2_test & v3_test ) == 0 &&
         check_line( t[1], t[2], v2_test | v3_test ) ) return true;

    // By now, we know that the triangle is not off to any side,
    // and that its sides do not penetrate the cube.  We must now
    // test for the cube intersecting the interior of the triangle.
    // We do this by looking for intersections between the cube
    // diagonals and the triangle...first finding the intersection
    // of the four diagonals with the plane of the triangle, and
    // then if that intersection is inside the cube, pursuing
    // whether the intersection point is inside the triangle itself.

    // To find plane of the triangle, first perform crossproduct on
    // two triangle side vectors to compute the normal vector.
    const norm = vec_cross( e[0], e[1] );

    // The normal vector "norm" X,Y,Z components are the coefficients
    // of the triangles AX + BY + CZ + D = 0 plane equation.  If we
    // solve the plane equation for X=Y=Z (a diagonal), we get
    // -D/(A+B+C) as a metric of the distance from cube center to the
    // diagonal/plane intersection.  If this is between -0.5 and 0.5,
    // the intersection is inside the cube.  If so, we continue by
    // doing a point/triangle intersection.
    // Do this for all four diagonals.
    const d = norm[0] * t[0][0] + norm[1] * t[0][1] + norm[2] * t[0][2];

    // if one of the diagonals is parallel to the plane, the other will
    // intersect the plane
    let denom = norm[0] + norm[1] + norm[2];
    // skip parallel diagonals to the plane; division by 0 can occur
    if ( Math.abs( denom ) > EPSILON ) {
        const dd = d / denom;
        const hitpp = [ dd, dd, dd ];
        if ( Math.abs( dd ) <= 0.5 &&
             point_triangle_intersection( hitpp, t, e ) ) return true;
    }
    denom = norm[0] + norm[1] - norm[2];
    if ( Math.abs( denom ) > EPSILON ) {
        const dd = d / denom;
        const hitpn = [ dd, dd, -dd ];
        if ( Math.abs( dd ) <= 0.5 &&
             point_triangle_intersection( hitpn, t, e ) ) return true;
    }
    denom = norm[0] - norm[1] + norm[2];
    if ( Math.abs( denom ) > EPSILON ) {
        const dd = d / denom;
        const hitnp = [ dd, -dd, dd ];
        if ( Math.abs( dd ) <= 0.5 &&
             point_triangle_intersection( hitnp, t, e ) ) return true;
    }
    denom = norm[0] - norm[1] - norm[2];
    if ( Math.abs( denom ) > EPSILON ) {
        const dd = d / denom;
        const hitnn = [ dd, -dd, -dd ];
        if ( Math.abs( dd ) <= 0.5 &&
             point_triangle_intersection( hitnn, t, e ) ) return true;
    }

    // No edge touched the cube; no cube diagonal touched the triangle.
    // We're done...there was no intersection.
    return false;
}


/* Method “Schawrz-Seidel”
 *
 * http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/
 * 2019. Hoetzlein, Rama. Voxelization of a triangle in 3D.
 */
function schwarz_seidel_test( v, e ) {
    const norm = vec_normalize( vec_cross( e[0], e[1] ) );

    //-- fast box-plane test
    const r = 0.5 * Math.abs( norm[0] ) + 0.5 * Math.abs( norm[1] ) + 0.5 * Math.abs( norm[2] );
    const s = norm[0] * ( 0.5 - v[0][0] ) + norm[1] * ( 0.5 - v[0][1] ) + norm[2] * ( 0.5 - v[0][2] );
    if ( Math.abs( s ) > r ) return false;

    //-- schwarz-seidel tests
    let c = [ norm[0] >= 0.0 ? 1.0 : -1.0,
              norm[1] >= 0.0 ? 1.0 : -1.0,
              norm[2] >= 0.0 ? 1.0 : -1.0 ];
    if ( -( -e[0][1] * v[0][0] + e[0][0] * v[0][1] ) * c[2] + Math.max( 0.0, -e[0][1] * c[2] ) + Math.max( 0.0, e[0][0] * c[2] ) < 0.0 ) return false;
    if ( -( -e[1][1] * v[1][0] + e[1][0] * v[1][1] ) * c[2] + Math.max( 0.0, -e[1][1] * c[2] ) + Math.max( 0.0, e[1][0] * c[2] ) < 0.0 ) return false;
    if ( -( -e[2][1] * v[2][0] + e[2][0] * v[2][1] ) * c[2] + Math.max( 0.0, -e[2][1] * c[2] ) + Math.max( 0.0, e[2][0] * c[2] ) < 0.0 ) return false;
    if ( -( -e[0][0] * v[0][2] + e[0][2] * v[0][0] ) * c[1] + Math.max( 0.0, -e[0][0] * c[1] ) + Math.max( 0.0, e[0][2] * c[1] ) < 0.0 ) return false;
    if ( -( -e[1][0] * v[1][2] + e[1][2] * v[1][0] ) * c[1] + Math.max( 0.0, -e[1][0] * c[1] ) + Math.max( 0.0, e[1][2] * c[1] ) < 0.0 ) return false;
    if ( -( -e[2][0] * v[2][2] + e[2][2] * v[2][0] ) * c[1] + Math.max( 0.0, -e[2][0] * c[1] ) + Math.max( 0.0, e[2][2] * c[1] ) < 0.0 ) return false;
    if ( -( -e[0][2] * v[0][1] + e[0][1] * v[0][2] ) * c[0] + Math.max( 0.0, -e[0][2] * c[0] ) + Math.max( 0.0, e[0][1] * c[0] ) < 0.0 ) return false;
    if ( -( -e[1][2] * v[1][1] + e[1][1] * v[1][2] ) * c[0] + Math.max( 0.0, -e[1][2] * c[0] ) + Math.max( 0.0, e[1][1] * c[0] ) < 0.0 ) return false;
    if ( -( -e[2][2] * v[2][1] + e[2][1] * v[2][2] ) * c[0] + Math.max( 0.0, -e[2][2] * c[0] ) + Math.max( 0.0, e[2][1] * c[0] ) < 0.0 ) return false;

    return true;
}


// go through the bounding box of the triangle in the voxel space and return
// the list of intersecting voxels
function search_all_voxels( tri, width, length, height, test_method ) {
    let result = [];

    // bounding box
    let min = [ width, length, height ];
    let max = [ 0, 0, 0 ];
    for ( let i = 0; i < 3; ++i ) {
        for ( let j = 0; j < 3; ++j ) {
            const v = Math.floor( tri[i][j] );
            if ( v < min[j] ) min[j] = v;
            if ( v > max[j] ) max[j] = v;
        }
    }
    for ( let j = 0; j < 3; ++j ) {
        if ( min[j] < 0 ) min[j] = 0;
    }
    if ( max[0] >=  width ) max[0] =  width - 1;
    if ( max[1] >= length ) max[1] = length - 1;
    if ( max[2] >= height ) max[2] = height - 1;

    // small triangle
    if ( min[0] == max[0] && min[1] == max[1] && min[2] == max[2] ) {
        return [ min ];
    }

    // compute triangle edges
    const e = [
        vec_sub( tri[1], tri[0] ),
        vec_sub( tri[2], tri[1] ),
        vec_sub( tri[0], tri[2] )
    ];

    for ( let x = min[0]; x <= max[0]; ++x ) {
        for ( let y = min[1]; y <= max[1]; ++y ) {
            for ( let z = max[2]; z >= min[2]; --z ) {
                // recenter on voxel’s center
                const voxel = [ x, y, z ];
                const trip = [
                    vec_sub( tri[0], voxel ),
                    vec_sub( tri[1], voxel ),
                    vec_sub( tri[2], voxel )
                ];
                if ( test_method( trip, e ) ) {
                    result.push( voxel );
                    break; // top reached
                }
            }
        }
    }
    return result;
}

// read the triangles in the text lines and scale them to the box
function read_triangles( lines, width, length, height ) {
    // parse the lines
    let triangles = [];
    let min = [  Infinity,  Infinity,  Infinity ];
    let max = [ -Infinity, -Infinity, -Infinity ];

    for ( let line of lines ) {
        let elts = line.split( /\s+/ );
        let tri = [];
        for ( let i = 0; i < 3; ++i ) {
            tri[i] = [];
            for ( let j = 0; j < 3; ++j ) {
                let coord = parseFloat( elts.shift() );
                if ( coord < min[j] ) min[j] = coord;
                if ( coord > max[j] ) max[j] = coord;
                tri[i][j] = coord;
            }
        }
        triangles.push( tri );
    }

    // scale to voxel space
    const ratio = [
        (  width - 1.0 ) / ( max[0] - min[0] ),
        ( length - 1.0 ) / ( max[1] - min[1] ),
        ( height - 1.0 ) / ( max[2] - min[2] )
    ];
    for ( let tri of triangles ) {
        for ( let i = 0; i < 3; ++i ) {
            for (let j = 0; j < 3; ++j ) {
                tri[i][j] = ( tri[i][j] - min[j] ) * ratio[j];
            }
        }
    }
    return triangles;
}

// find the voxels
function find_voxels( triangles, width, length, height, method, submethod ) {
    let voxels = [];
    for ( let tri of triangles ) {
        voxels = voxels.concat( method( tri, width, length, height, submethod ) );
    }
    return voxels;
}

// make a bitmap of voxels
function make_bitmap( voxels, width, length, height ) {
    let bitmap = [];
    for ( let x = 0; x < width; ++x ) {
        bitmap[x] = [];
        for ( let y = 0; y < length; ++y ) {
            bitmap[x][y] = 0;
        }
    }

    let count = 0;
    for ( let vox of voxels ) {
        const xx = Math.floor( vox[0] );
        const yy = Math.floor( vox[1] );
        const zz = Math.floor( vox[2] );
        if ( xx >= 0 && xx < width &&
             yy >= 0 && yy < length &&
             zz >= 0 && zz < height ) {
            if ( zz > bitmap[xx][yy] ) {
                bitmap[xx][yy] = zz;
                ++count;
            }
        }
    }
    //console.log( 'pixels ' + count );
    return bitmap;
}

function tri2png( file, canvas, params, blob_cb ) {
    // method parameter
    let method = search_all_voxels;
    let submethod = tri_box_overlap;
    if ( params.method == 'voorhies' ) {
        submethod = t_c_intersection;
    } else if ( params.method == "schwarz" ) {
        submethod = schwarz_seidel_test;
    } else if ( params.method == "akenine" ) {
        submethod = tri_box_overlap;
    } else if ( params.method == "raster" ) {
        method = voxelize_triangle;
        submethod = null;
    }

    // read the file
    let reader = new FileReader();
    reader.onload = function( progress ) {
        // triangles
        const lines = this.result.split( "\n" );
        const triangles = read_triangles( lines, params.width, params.height, params.depth );
        //console.log( 'triangles ' + triangles.length );

        // voxels
        const voxels = find_voxels( triangles, params.width, params.height, params.depth, method, submethod );
        //console.log( 'voxels ' + voxels.length );

        // bitmap
        const bitmap = make_bitmap( voxels, params.width, params.height, params.depth );

        // png
        const ctx = canvas.getContext( "2d" );
        const image = ctx.createImageData( params.width, params.height );
        const color_ratio = 255.0 / params.depth;
        for ( let x = 0, offset = 0; x < params.width; ++x ) {
            for ( let y = 0; y < params.height; ++y ) {
                const c = Math.floor( bitmap[x][y] * color_ratio );
                image.data[ offset++ ] = c;   // R
                image.data[ offset++ ] = c;   // G
                image.data[ offset++ ] = c;   // B
                image.data[ offset++ ] = 255; // A
            }
        }
        ctx.putImageData( image, 0, 0 );
        // send png back
        canvas.toBlob( blob_cb );
    };
    reader.readAsText( file );
}
